package astlink

import (
	"go/ast"
	"log"

	"gitlab.com/dfinkel/wasm-test/util"
)

// MarkComments splits all comments into their own div nodes and adds class "Comment"
func (st *State) MarkComments() {
	for _, cgrp := range st.fast.Comments {
		for _, c := range cgrp.List {
			st.markComment(c)
		}
	}
}

func (st *State) markComment(c *ast.Comment) {
	if _, err := st.setRangeClass(util.Span, "Comment", c); err != nil {
		log.Printf("failed to split out comment node (%q): %s",
			c.Text, err)
	}
}
