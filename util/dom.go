package util

// HTMLElemType is a string for the element type (A for anchors (links), DIV
// for div, etc.)
type HTMLElemType string

// HTMLElemType constants
const (
	Div    HTMLElemType = "DIV"
	Span   HTMLElemType = "SPAN"
	Anchor HTMLElemType = "A"
)

// DomDocument implementations wrap the top-level document object
type DomDocument interface {
	CreateElement(Tag HTMLElemType) DomNode
	CreateTextNode(contents string) DomNode
	FindByID(id string) DomNode
}

// DomNode provides a wrapper around the DOM to support both the syscall/js
// package-based interaction from wasm and the golang.org/x/net/html-based
// interaction with building a static HTML page
type DomNode interface {
	TagName() string
	SetText(string)

	ChildText() string
	ChildLength() int
	// Returns parent, or nil if it doesn't have a parent
	Parent() DomNode

	AppendChild(DomNode)
	ReplaceChild(newNode, oldNode DomNode)
	// Split splits the children of the current node in such a way as to
	// separate out the characters on the half-open interval [start, end).
	Split(start, end int) (DomNode, error)
	SetAttr(name, val string)
	GetAttr(name string) string

	SetClass(name string)
}
