package astlink

import (
	"go/ast"
	"go/token"

	"gitlab.com/dfinkel/wasm-test/util"
)

// State holds the mapping of import to import name
type State struct {
	m map[string]*ast.ImportSpec
	// impFrag is a map from import name to URL fragment
	impFrag map[string]string

	objFrag map[*ast.Object]string

	constVals map[*ast.Object]string

	// top-level DOM document object
	doc util.DomDocument

	// outermost node containing go source
	container util.DomNode

	// fileset into which the file was parsed
	fs *token.FileSet
	// fast is the ast of the parsed file
	fast *ast.File
}

// NewState constructs a new State
func NewState(doc util.DomDocument, div util.DomNode, fs *token.FileSet, fast *ast.File) *State {
	return &State{
		m:         make(map[string]*ast.ImportSpec, len(fast.Imports)),
		impFrag:   make(map[string]string, len(fast.Imports)),
		objFrag:   make(map[*ast.Object]string, len(fast.Decls)),
		constVals: make(map[*ast.Object]string),
		doc:       doc,
		container: div,
		fs:        fs,
		fast:      fast,
	}
}
