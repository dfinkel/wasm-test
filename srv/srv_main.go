package main

import "log"
import "net/http"
import "flag"

type logHandler struct {
	inner http.Handler
}

func (l *logHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	log.Print("received request for ", r.RequestURI)
	l.inner.ServeHTTP(w, r)
}

func main() {
	flag.Parse()
	// Serve the current directory.
	http.Handle("/", &logHandler{http.FileServer(http.Dir("."))})
	if err := http.ListenAndServe(":8080", nil); err != nil {
		log.Fatalf("Error in http server: %s\n", err)
	}
}
