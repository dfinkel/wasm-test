package astlink

import (
	"fmt"
	"go/ast"
	"log"
	"net/url"
	"path"
	"strconv"
	"strings"

	"gitlab.com/dfinkel/wasm-test/util"
)

func linkedImport(doc util.DomDocument, imp *ast.ImportSpec, frag string) util.DomNode {
	u := pkgDoc(imp.Path.Value)
	txtNode := doc.CreateTextNode(imp.Path.Value)
	anch := util.NewAnchor(doc, u, txtNode)
	anch.SetAttr("name", frag)
	anch.SetClass("string-import")
	return anch

}

func importFragment(imp *ast.ImportSpec) string {
	unquoted, _ := strconv.Unquote(imp.Path.Value)
	return "imp-" + unquoted
}

func pkgDoc(pth string) *url.URL {
	strippedPath, _ := strconv.Unquote(pth)
	if strings.Contains(strippedPath, ".") {
		return &url.URL{
			Scheme: "https",
			Host:   "godoc.org",
			Path:   strippedPath,
		}
	}
	return &url.URL{
		Scheme: "https",
		Host:   "golang.org",
		Path:   "/pkg/" + strippedPath,
	}
}

// SetImportMarks uses data from fs and fast to create html links to the
// relevant documentation.
func (st *State) SetImportMarks() error {

	for _, imp := range st.fast.Imports {
		if imp.Pos().IsValid() {
			importPath, unquotErr := strconv.Unquote(imp.Path.Value)
			if unquotErr != nil {
				log.Printf("failed to unquote text %q: %s", imp.Path.Value, unquotErr)
				return nil
			}
			impFrag := importFragment(imp)

			newAnchor := linkedImport(st.doc, imp, impFrag)
			newAnchor.SetAttr("title", "imports "+imp.Path.Value)

			pathAnchor, pathSplitErr := st.setRangeClass(util.Anchor, "string-import", imp.Path)
			if pathSplitErr != nil {
				log.Printf("failed to mark import of %s as an anchor: %s", imp.Path.Value, pathSplitErr)
				return pathSplitErr
			}

			pathAnchor.SetAttr("name", impFrag)

			name := path.Base(importPath)
			// If there's an identifier, use it.
			if imp.Name != nil {
				name = imp.Name.Name
				_, idErr := st.setRangeClass(util.Span, "Identifier", imp.Name)
				if idErr != nil {
					return fmt.Errorf("failed to mark import ID %q with error: %s", imp.Name.Name, idErr)
				}
			}
			st.m[name] = imp
			st.impFrag[name] = impFrag
		}
	}
	return nil
}
