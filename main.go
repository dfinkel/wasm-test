// +build js,wasm

package main

import (
	"go/ast"
	"go/format"
	"go/parser"
	"go/token"
	"log"
	"strings"
	"syscall/js"

	"gitlab.com/dfinkel/wasm-test/astlink"
	"gitlab.com/dfinkel/wasm-test/util"
)

func getFileAst(s string) (*token.FileSet, *ast.File, error) {
	fs := token.NewFileSet()
	fast, err := parser.ParseFile(fs, "foo.go", s, parser.ParseComments)
	return fs, fast, err

}

func updateTextHighlighting(doc util.DomDocument, bar util.DomNode) {
	// we need to strip out any &nbsp (non-breaking spaces (U+00A0))
	rawText := strings.Map(func(r rune) rune {
		if r == '\u00A0' {
			return ' '
		}
		return r
	}, bar.ChildText())
	txtBytes, fmtErr := format.Source([]byte(rawText))
	if fmtErr != nil {
		log.Printf("failed to format file: %s", fmtErr)
		return
	}
	txt := strings.ReplaceAll(string(txtBytes), "\t", "        ")

	fs, fast, err := getFileAst(txt)
	if err != nil {
		log.Printf("unable to parse go source: %s", err)
		return
	}

	newDiv := util.NewDIV(doc, "bar", strings.ReplaceAll(txt, "        ", "\u00A0      \u00A0"))

	st := astlink.NewState(doc, newDiv, fs, fast)
	st.SetImportMarks()
	st.MarkComments()
	st.MarkRefs()

	// Swap the old div for the new div
	divParent := bar.Parent()
	divParent.ReplaceChild(newDiv, bar)
}

func getButton(doc js.Value) js.Value {
	return doc.Call("getElementById", "runButton")
}

const tmplGoSrc = `package main
import "log"
import "github.com/spf13/viper"

var foobar = "abc"

const l = 32

var j = l + 30

var s = foobar + "def"

func main() {
	conf := viper.GetConfig()

	log.Println("hello world ", conf.GetString("foobar"))
}
`

func main() {
	log.SetFlags(log.Lshortfile | log.Lmicroseconds | log.Ldate)
	doc := util.NewJSDomDocument()

	bar := doc.FindByID("bar")
	bar.SetText(tmplGoSrc)
	bar.SetAttr("contenteditable", "")
	bar.SetAttr("spellcheck", "false")

	buttonChan := make(chan js.Value, 1)
	cb := js.FuncOf(func(this js.Value, args []js.Value) interface{} {
		select {
		case buttonChan <- this:
		default:
		}
		return nil
	})
	defer cb.Release()
	// We'll override the run() function for now.
	js.Global().Set("run", cb)

	go func() {
		updateTextHighlighting(doc, bar)
		for {
			select {
			case _, open := <-buttonChan:
				if !open {
					return
				}
				txtArea := doc.FindByID("bar")
				updateTextHighlighting(doc, txtArea)
			}
		}
	}()

	// block indefinitely as everything else is taking place in another
	// goroutine
	<-make(chan struct{})
}
