package astlink

import (
	"fmt"
	"go/ast"
	"go/token"
	"strconv"

	"gitlab.com/dfinkel/wasm-test/util"
)

// MarkRefs adds references to references to the imported packages in a
// file.
func (st *State) MarkRefs() {
	st.setKeywordTokenClass(token.PACKAGE, st.fast.Package)
	st.setRangeClass(util.Span, "Identifier", st.fast.Name)
	for _, a := range st.fast.Decls {
		switch z := a.(type) {
		case *ast.GenDecl:
			st.markDecl(z)
		case *ast.FuncDecl:
			st.markFuncDecl(z)
		}
	}
}

func (st *State) markDecl(z *ast.GenDecl) {
	st.setKeywordTokenClass(z.Tok, z.TokPos)
	switch z.Tok {
	// We already took care of imports, so don't try to do anything.
	case token.IMPORT:
		return
	case token.VAR, token.CONST:
		st.markVarDecl(z, z.Tok == token.CONST)
	case token.TYPE:
		st.markTypeDecl(z)
	}
}

func (st *State) markNewIdent(i *ast.Ident) {
	id := genID(i.Name, i)
	a, stErr := st.setRangeClass(util.Anchor, "Identifier", i)
	if stErr != nil {
		panic(stErr)
	}
	a.SetAttr("name", id)
	st.objFrag[i.Obj] = id

}

func (st *State) markVarDecl(a *ast.GenDecl, isConst bool) {
	var lastVals []ast.Expr
	// We're now left with variable and constant declarations, either way,
	// []spec is of type []*ValueSpec.
	for iotaNum, sp := range a.Specs {
		sp := sp.(*ast.ValueSpec)
		vals := sp.Values
		if len(vals) == 0 {
			vals = lastVals
		} else {
			lastVals = vals
		}
		for i, n := range sp.Names {
			st.markNewIdent(n)
			if !isConst {
				continue
			}

			switch v := vals[i].(type) {
			case *ast.BasicLit:
				st.constVals[n.Obj] = v.Value
			case *ast.Ident:
				if v.Obj != nil && v.Obj.Type == ast.Con {
					st.constVals[n.Obj] = st.constVals[v.Obj]
				} else if v.Name == "iota" {
					st.constVals[n.Obj] = strconv.Itoa(iotaNum)
				}

			}
		}
		for _, v := range sp.Values {
			st.markExpr(v)
		}
		st.markExpr(sp.Type)
	}

}

func (st *State) offsetOf(pos token.Pos) int {
	return st.fs.Position(pos).Offset
}
func (st *State) offsetRangeOf(n ast.Node) (int, int) {
	return st.offsetOf(n.Pos()), st.offsetOf(n.End())
}

func (st *State) setRangeClass(elemType util.HTMLElemType, class string, n ast.Node) (util.DomNode, error) {
	start, end := st.offsetRangeOf(n)
	return util.SetRangeClass(elemType, st.doc, st.container, class, start, end)
}

func (st *State) markTypeDecl(a *ast.GenDecl) {
	st.setKeywordTokenClass(a.Tok, a.TokPos)
	for _, z := range a.Specs {
		ts := z.(*ast.TypeSpec)
		frag := genID(ts.Name.Name, a)
		st.objFrag[ts.Name.Obj] = frag
		a, err := st.setRangeClass(util.Anchor, "Type", ts.Name)
		if err != nil {
			return
		}
		a.SetAttr("name", frag)
		st.markExpr(ts.Type)

	}
}

func (st *State) markExpr(e ast.Expr) {
	ast.Inspect(e, func(n ast.Node) bool {
		switch ln := n.(type) {
		case nil:
			return false
		case *ast.Ident:
			{
				class := "Identifier"
				if ln.Name == "iota" {
					class = "Statement"
				}
				if ln.Obj == nil {
					st.setRangeClass(util.Span, class, ln)
					return false
				}
				obj := ln.Obj
				frag := ""
				if objfrag, ok := st.objFrag[obj]; ok {
					frag = objfrag
				}
				alt := "type: " + obj.Kind.String()
				switch obj.Kind {
				case ast.Con:
					class = "Constant"
					alt = "const val = " + st.constVals[obj]
				case ast.Fun:
					class = "Function"
				case ast.Typ:
					class = "Type"
				case ast.Pkg:
					if pkgFrag, hasFrag := st.impFrag[ln.Name]; hasFrag {

						frag = pkgFrag

					}
				case ast.Lbl, ast.Bad:
				}
				a, identErr := st.setRangeClass(util.Anchor, class, ln)
				if identErr != nil {
					panic(identErr)
				}

				if frag != "" {
					a.SetAttr("href", "#"+frag)
				}
				if alt != "" {
					a.SetAttr("title", alt)
				}
			}
		case *ast.BasicLit:
			st.setRangeClass(util.Span, "Constant", ln)
		case *ast.BinaryExpr:
			st.setKeywordTokenClass(ln.Op, ln.OpPos)
		case *ast.SelectorExpr:
			st.setKeywordTokenClass(token.PERIOD, ln.X.End())
		case *ast.TypeAssertExpr:
			if ln.Type == nil {
				st.setKeywordTokenClass(token.TYPE, ln.Lparen+1)
			}
		case *ast.StructType:
			st.setKeywordTokenClass(token.STRUCT, ln.Struct)
		case *ast.MapType:
			st.setKeywordTokenClass(token.MAP, ln.Map)
			st.markExpr(ln.Key)
			st.markExpr(ln.Value)
		case *ast.InterfaceType:
			st.setKeywordTokenClass(token.INTERFACE, ln.Interface)
		case *ast.FuncType:
			st.setKeywordTokenClass(token.FUNC, ln.Func)
		case *ast.FuncLit:
			st.markExpr(ln.Type)
			st.markStatement(ln.Body)
			return false
		}
		return true
	})
}

func (st *State) markStatement(s ast.Stmt) {
	switch stmt := s.(type) {
	case *ast.ExprStmt:
		st.markExpr(stmt.X)
	case *ast.AssignStmt:
		st.setKeywordTokenClass(stmt.Tok, stmt.TokPos)
		for _, r := range stmt.Rhs {
			st.markExpr(r)
		}
		if stmt.Tok == token.DEFINE {
			for _, i := range stmt.Lhs {
				if id, isid := i.(*ast.Ident); isid {
					st.markNewIdent(id)
				} else {
					st.markExpr(i)
				}
			}
		} else {
			for _, i := range stmt.Lhs {
				st.markExpr(i)
			}
		}
	case *ast.GoStmt:
		st.setKeywordTokenClass(token.GO, stmt.Go)
		st.markExpr(stmt.Call)
	case *ast.DeferStmt:
		st.setKeywordTokenClass(token.DEFER, stmt.Defer)
		st.markExpr(stmt.Call)
	case *ast.IfStmt:
		st.setKeywordTokenClass(token.IF, stmt.If)
		st.markStatement(stmt.Init)
		st.markExpr(stmt.Cond)
		st.markStatement(stmt.Body)
		st.markStatement(stmt.Else)
	case *ast.BlockStmt:
		for _, s := range stmt.List {
			st.markStatement(s)
		}
	case *ast.BranchStmt:
		st.setKeywordTokenClass(stmt.Tok, stmt.TokPos)
		if stmt.Label != nil {
			st.markExpr(stmt.Label)
		}
	case *ast.SwitchStmt:
		st.setKeywordTokenClass(token.SWITCH, stmt.Switch)
		st.markStatement(stmt.Init)
		st.markExpr(stmt.Tag)
		st.markStatement(stmt.Body)
	case *ast.CaseClause:
		if stmt.List == nil {
			st.setKeywordTokenClass(token.DEFAULT, stmt.Case)
		} else {
			st.setKeywordTokenClass(token.CASE, stmt.Case)
			for _, e := range stmt.List {
				st.markExpr(e)
			}
		}
		for _, s := range stmt.Body {
			st.markStatement(s)
		}
	case *ast.CommClause:
		if stmt.Comm == nil {
			st.setKeywordTokenClass(token.DEFAULT, stmt.Case)
		} else {
			st.setKeywordTokenClass(token.CASE, stmt.Case)
			st.markStatement(stmt.Comm)
		}
		for _, s := range stmt.Body {
			st.markStatement(s)
		}
	case *ast.DeclStmt:
		st.markDecl(stmt.Decl.(*ast.GenDecl))
	case *ast.ForStmt:
		st.setKeywordTokenClass(token.FOR, stmt.For)
		st.markStatement(stmt.Init)
		st.markExpr(stmt.Cond)
		st.markStatement(stmt.Post)
		st.markStatement(stmt.Body)
	case *ast.IncDecStmt:
		st.setKeywordTokenClass(stmt.Tok, stmt.TokPos)
		st.markExpr(stmt.X)
	case *ast.LabeledStmt:
		st.markStatement(stmt.Stmt)
		if stmt.Label != nil && stmt.Label.Obj != nil {
			st.markNewIdent(stmt.Label)
		}
	case *ast.RangeStmt:
		st.setKeywordTokenClass(token.FOR, stmt.For)
		if stmt.Tok == token.DEFINE {
			if keyID, keyIsID := stmt.Key.(*ast.Ident); keyIsID {
				st.markNewIdent(keyID)
			} else {
				st.markExpr(stmt.Key)
			}
			if valID, valIsID := stmt.Value.(*ast.Ident); valIsID {
				st.markNewIdent(valID)
			} else {
				st.markExpr(stmt.Value)
			}
		}
		st.markExpr(stmt.X)
		st.markStatement(stmt.Body)
	case *ast.ReturnStmt:
		st.setKeywordTokenClass(token.RETURN, stmt.Return)
		for _, e := range stmt.Results {
			st.markExpr(e)
		}
	case *ast.SelectStmt:
		st.setKeywordTokenClass(token.SELECT, stmt.Select)
		st.markStatement(stmt.Body)
	case *ast.SendStmt:
		st.setKeywordTokenClass(token.ARROW, stmt.Arrow)
		st.markExpr(stmt.Chan)
		st.markExpr(stmt.Value)
	case *ast.TypeSwitchStmt:
		st.setKeywordTokenClass(token.SWITCH, stmt.Switch)
		st.markStatement(stmt.Init)
		st.markStatement(stmt.Assign)
		st.markStatement(stmt.Body)

	}
}

func (st *State) setKeywordTokenClass(tok token.Token, pos token.Pos) {
	util.SetRangeClass(util.Span, st.doc, st.container, "Statement",
		st.offsetOf(pos), st.offsetOf(pos)+len(tok.String()))
}

func genID(name string, n ast.Node) string {
	pos := n.Pos()
	return fmt.Sprintf("%s_%04d", name, int(pos))
}

func (st *State) markFuncDecl(f *ast.FuncDecl) {
	name := f.Name.Name
	frag := genID(name, f)
	st.objFrag[f.Name.Obj] = frag

	a, err := st.setRangeClass(util.Anchor, "Identifier", f.Name)
	if err != nil {
		return
	}
	a.SetAttr("name", frag)
	_, typeKWErr := util.SetRangeClass(util.Span, st.doc, st.container, "Statement",
		st.offsetOf(f.Type.Func), st.offsetOf(f.Type.Func)+len("func"))
	if typeKWErr != nil {
		return
	}

	st.markStatement(f.Body)
}
